
var data = [
  "Jeff Bezos",
  "Bill Gates",
  "Warren Buffet",
  "Bernard Arnult",
  "Carlos Slim Helu",
  "Amancio Ortega",
  "Larry Ellison",
  "Mark Zuckerberg",
  "Michael Bloomberg",
  "Larry Page",
];

window.addEventListener("DOMContentLoaded", dataView());

function dataView() {
  let view = data.map(
    (index, value ) =>
      `
    <div class="input-text">
            <p class="text">` + index + `</p>
        <div class="btns">
            <button class="btn" id="delete" onclick="remove(` + value +`)"><i class="fas fa-trash-alt"></i></button>
            <button class="btn" id="edit" onclick="edit(` + value + `)"><i class="fas fa-edit"></i> </button>
        </div>             
    </div>
    `
  );
  view = view.join("");
  document.getElementById("items").innerHTML = view;
}

function remove(value) {
  data.splice(value, 1);
  swal.fire({
    icon: "success",
    title: "Informations successfully deleted",
    showConfirmButton: false,
    timer: 900,
  });
  dataView();
}
var editId = -1; 
document.getElementById("save").addEventListener("click", Add);
var input = document.getElementById("add-input");
function Add() {
    
  var inputData = input.value;
  if (inputData.length > 0) {
    if (editId > 0) {
        editId = editId - 1
      data.splice(editId, true, inputData.trim());
      input.value = "";
      document.getElementById("save").innerHTML =
        ' <i class="far fa-plus-square" id="btn-icon"></i>';
        editId = -1;
      dataView();

      swal.fire({
        icon: "success",
        title: "Changes succesfuly saved",
        showConfirmButton: false,
        timer: 900,
      });
        
    } else {
      console.log(editId);
      data.push(inputData.trim());
      input.value = "";
       editId = -1;

       swal.fire({
        icon: "success",
        title: "New information successfully saved ",
        showConfirmButton: false,
        timer: 900,
      });

      dataView();
    }
  } else {
      
    swal.fire({
      icon: "error",
      title: "Input value is empty",
      text: "Please enter some information ",
      showConfirmButton: false,
      timer: 900,
    });
  }
}

function edit(index) {
  input.value = data[index];
  let icon = document.getElementById("btn-icon");
  icon.remove();
  document.getElementById("save").innerHTML =
    ' </i> <i class="fas fa-check" id="btn-icon"></i>';
  editId = index+1;
}
